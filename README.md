# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Colorful painter                                 | 1~5%      | Y         |


---

### How to use 

    這次的 WebCanvas 我實作的功能如下:
    1. Cursor icon: 滑鼠游標在點入 Pen、ColorPainter、Eraser、Circle、Rectangle、Triangle 時共 6 種的滑鼠圖片會改變

    2. Pen (固定顏色的畫筆)、Colorful Painter (會隨著畫動軌跡改變顏色的畫筆)、Color (調整顏色)、Line Width (畫筆粗細度)
    畫筆粗細度有 1 到 5 可以選。

    3. Eraser (橡皮擦)、Eraser Size (橡皮擦大小)
    橡皮擦的大小有 20 和 40 可以選。

    4. Circle (畫圓形)、Rectangle (畫長方形)、Triangle (畫三角形) 
    只能畫出黑色實心的圖形，但圖形的外緣是依據當時畫的時候的顏色而定。

    5. Text (文字方塊)、調整文字字形的功能、調整文字大小的功能
    英文文字時字形和大小會隨著選取的不同而有不一樣的結果。

    6. Undo (回上一步)、Redo (回下一步)、Refresh (清空畫布)

    7. Image upload (檔案上傳)、 Image download (檔案下載) 
    (A) 檔案上傳時會根據原圖大小直接上傳，所以可能會有圖片超出畫布的情形。
    (B) 檔案下載時會以 myImage.jpg 下載。

### Function description

    我的 bonus function 是 Color Painter，在使用 color painter 的時候，畫動的軌跡顏色會隨著游標移動而彩色地改變。

### Gitlab page link

    https://kaipochang.gitlab.io/as_01_webcanvas 


<style>
table th{
    width: 100%;
}
</style>


