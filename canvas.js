/* 初始化 */
var canvas = document.getElementById('draw'); // 取得畫布元素
var ctx = canvas.getContext('2d'); // 使用2D繪圖
var draw = false; // 是否要繪圖
var tempPos = [0, 0]; // 繪圖起始位置
var mode = "";
/* 畫筆顏色、粗細*/
var color = document.getElementById("color");
var lineWidth = document.getElementById("lineWidth");
ctx.strokeStyle = color.nodeValue;
ctx.lineWidth = lineWidth.value;
/* 橡皮擦 */
var eraser = document.getElementById("eraser");
var eraserWidth = document.getElementById("eraserWidth");
/* 畫圖記錄點 */
var x4draw = 0;
var y4draw = 0;
var dataURL = canvas.toDataURL();
/* Redo 以及 Undo*/
var cPushArray = new Array();
var cRedoArray = new Array();
var cStep = -1;

var hue = 0;

dataURL = ctx.getImageData(0, 0, canvas.width, canvas.height);
cPushArray.push(dataURL);

function loadImage(url){
    ctx.putImageData(url, 0, 0);
}
/* Redo 以及 Undo 的 function*/
function cPush() {
    cStep++;
    console.log(cStep);
    cPushArray.push(canvas.toDataURL());
}
function cUndo() {
    if (cPushArray.length > 1) {
        cRedoArray.push(cPushArray.pop());
        loadImage(cPushArray[cPushArray.length-1]);
    }
}
function cRedo() {
    if (cRedoArray.length > 0) {
        cPushArray.push(cRedoArray.pop());
        loadImage(cPushArray[cPushArray.length-1]);
    }
}

/* Upload images*/
var imageLoader = document.getElementById('imageLoader');
imageLoader.addEventListener('change', handleImage, false);


function handleImage(e){
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            canvas.width = img.width;
            canvas.height = img.height;
            ctx.drawImage(img,0,0);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);     
}
/* Download images*/
download_img = function(el) {
    var image = canvas.toDataURL("image/jpg");
    el.href = image;
  };

/* TEXT input*/
function keyboardclick(event){
    if (event.keyCode == 13){
        var textinput = document.getElementById("box");
        let textfont = document.getElementById("textFont").value;
        let textsize = document.getElementById("textSize").value;
        ctx.font = textsize.toString() + "px" + " " + textfont;
        let x = textinput.getBoundingClientRect().left-canvas.getBoundingClientRect().left;
        let y = textinput.getBoundingClientRect().top-canvas.getBoundingClientRect().top;
        ctx.fillText(textinput.value, x, y);
    }
    textinput.value = "";
    textinput.style.display = "none";
}

function changeMode(m){
    mode = m;
    if (mode == "refresh"){
        ctx.clearRect (0, 0, canvas.width, canvas.height);
        cPush();
    }
    else if (mode == "redo"){
        cRedo();
    }
    else if (mode == "undo"){
        cUndo();
    }
    else if (mode == "pen" || mode == "colorfulPainter"){
        canvas.style.cursor = "url('pen.png'), default";
    }
    else if (mode == "eraser"){
        canvas.style.cursor = "url('eraser.png'), default";
    }
    else if (mode == "circle"){
        canvas.style.cursor = "url('circle.png'), default";
    }
    else if (mode == "rectangle"){
        canvas.style.cursor = "url('rectangle.png'), default";
    }
    else if (mode == "triangle"){
        canvas.style.cursor = "url('triangle.png'), default";
    }
}

canvas.addEventListener('mousedown', function(e){
    draw = true;
    tempPos = [e.clientX-canvas.offsetLeft, e.clientY-canvas.offsetTop];
    var img = new Image();
    if (mode == "circle"|| mode == "rectangle" || mode == "triangle"){
        x4draw = tempPos[0];
        y4draw = tempPos[1];
    }
    else if (mode == "text"){
        document.getElementById("box").style.display = "block";
        document.getElementById("box").style.top = e.clientY+"px";
        document.getElementById("box").style.left = e.clientX+"px";
    }
});

canvas.addEventListener('mousemove', function(e){
    if(draw){
        var newPos = [e.clientX-canvas.offsetLeft, e.clientY-canvas.offsetTop]; // 取得結束位置
        if (mode == "colorfulPainter") {
            ctx.strokeStyle = `hsl(${hue}, 100%, 50%)`;   
            hue++;
            if (hue >= 360) {
              hue = 0;
            }
        }
        // 判斷橡皮擦是否啟用
        if (mode == "eraser") {
            var _eraserWidth = eraserWidth.value;
            ctx.clearRect(tempPos[0] - (_eraserWidth / 2), tempPos[1] - (_eraserWidth / 2), _eraserWidth, _eraserWidth);
        }
        else if (mode == "pen" || mode == "colorfulPainter"){
          ctx.beginPath(); // 取得繪畫路徑
          ctx.moveTo(tempPos[0], tempPos[1]); // 移動到起始位置
          ctx.lineTo(newPos[0], newPos[1]); // 畫線到結束的位置
          ctx.closePath(); // 關閉繪畫的路徑
          ctx.stroke(); // 畫線
        }
        else if (mode == "circle"){
            loadImage(dataURL);
            ctx.beginPath();
            let x = Math.abs(x4draw-newPos[0]);
            let y = Math.abs(y4draw-newPos[1])
            let z = Math.sqrt(Math.pow(x,2)+Math.pow(y,2));
            ctx.arc(x4draw, y4draw, z, 0, 2 * Math.PI);
            ctx.fill();
            ctx.closePath(); // 關閉繪畫的路徑
            ctx.stroke();
        }
        else if (mode == "rectangle"){
            loadImage(dataURL);
            ctx.beginPath();
            ctx.rect(x4draw, y4draw, newPos[0]-x4draw, newPos[1]-y4draw);
            ctx.fill();
            ctx.closePath(); // 關閉繪畫的路徑
            ctx.stroke();
        }
        else if (mode == "triangle"){
            loadImage(dataURL);
            ctx.beginPath();
            ctx.moveTo(x4draw, y4draw);
            ctx.lineTo(newPos[0], newPos[1]);
            ctx.lineTo(x4draw, newPos[1]);
            ctx.fill();
            ctx.closePath(); // 關閉繪畫的路徑
            ctx.stroke();
        }
        tempPos = newPos; // 記錄下最後的位置為目前的位置
    }
});

canvas.addEventListener('mouseup', function(){
    draw = false;
    dataURL = ctx.getImageData(0, 0, canvas.width, canvas.height);
    cPushArray.push(dataURL);
    cRedoArray = [];
    //cStep = cPushArray.length-1;
});

// 改變畫筆顏色
color.addEventListener('input', function(){ 
    ctx.strokeStyle = color.value;
})

// 改變畫筆粗細
lineWidth.addEventListener('input', function(){
   ctx.lineWidth = lineWidth.value;
})
